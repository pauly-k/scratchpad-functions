import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { initialize_user_post_titles, set_post_id } from "./http/http.trigger";
import { postCreateHandler, postUpdatedHandler, postDeletedHandler } from "./triggers/post.trigger";

admin.initializeApp(functions.config().firebase);

/*************'
  Triggers
*************/

export const userPost_onCreate = functions.firestore
  .document("users/{userId}/posts/{postId}")
  .onCreate(postCreateHandler);

export const userPost_onUpdate = functions.firestore
  .document("users/{userId}/posts/{postId}")
  .onUpdate(postUpdatedHandler);

export const userPost_onDelete = functions.firestore
  .document("users/{userId}/posts/{postId}")
  .onDelete(postDeletedHandler);

/*************'
  HTTP 
*************/

export const initPostTitles = functions.https.onRequest(
  async (request, response) => {
    try {
      await initialize_user_post_titles();
      return response.send("post initialized");
    } catch (err) {
      return response.status(500).send(err);
    }
  }
);

export const InitPostIds = functions.https.onRequest(async (request, response) => {
	try {
		await set_post_id()
		return response.send('post ids set')
	} catch (err) {
		return response.status(500).send(err)
	}
})

// export const helloWorld = functions.https.onRequest((req, res) => {
// 	res.send("hello world")
// })
