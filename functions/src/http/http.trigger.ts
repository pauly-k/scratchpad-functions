import * as admin from "firebase-admin";

export async function initialize_user_post_titles() {
  const userDcos = await get_user_docs();

  for (const userDoc of userDcos) {
    await set_user_post_titles(userDoc.id);
  }
}

export async function set_post_id() {
  const userDcos = await get_user_docs();
  for (const userDoc of userDcos) {
    await add_documentId_to_post(userDoc);
  }
}

/***************
 implementation  
 *************/



async function get_user_docs() {
  const snapshot = await admin
    .firestore()
    .collection("users")
    .get();
  return snapshot.docs;
}

async function set_user_post_titles(
  userId: string
) {
  const db = admin.firestore()
  const titlesDoc = await getTitlesDoc(userId)
  const postsQuerySnap = await db.collection(`users/${userId}/posts`).get()


  const post_titles = postsQuerySnap.docs.map(doc => ({
    id: doc.id,
    title: doc.data().title,
    updated: doc.data().updated
  }));

  await titlesDoc.ref.update({
    post_titles,
    post_title_count: post_titles.length,
    updated: Date.now()
  });
  return post_titles.length;
}

async function getTitlesDoc(userId: string) {
  const db = admin.firestore()
  const doc_path = `users/${userId}/post_titles/titles`
  const titlesDoc = await db.doc(doc_path).get()
  if (!titlesDoc.exists) {
    await titlesDoc.ref.set({
      titles: [],
      updated: Date.now()
    })
  }
  return db.doc(doc_path).get()
}

async function add_documentId_to_post(
  userDoc: FirebaseFirestore.QueryDocumentSnapshot
) {
  const postsSnapshot = await userDoc.ref.collection("posts").get();

  const promises: any[] = [];
  postsSnapshot.forEach(doc => {
    promises.push(doc.ref.update({ id: doc.id }));
  });
  return Promise.all(promises);
}