import * as admin from "firebase-admin";

export function postUnchanged(before: any, after: any) {
  return before.title === after.title && before.markdwon === after.markdwon;
}

export async function setPostTitles(userId: string, postId: string, remove: boolean = false) {
  const doc_path = `users/${userId}/posts/${postId}`
  const postDoc = await admin
    .firestore()
    .doc(doc_path)
    .get();

  const post = postDoc.data() as any;
  const titleData = {
    id: post.id,
    title: post.title,
    updated: post.updated
  };

  const titlesDoc = await getPostTitlesDoc(userId);
  const val = titlesDoc.data() as any;
  let post_titles = val.post_titles as any[];

  const index = post_titles.findIndex(x => x.id === post.id);

  if (remove) {
    post_titles = [...post_titles.slice(0, index), ...post_titles.slice(index + 1)];
  } else {
    if (index < 0) {
      post_titles = [...post_titles, titleData];
    } else {
      post_titles = [
        ...post_titles.slice(0, index),
        titleData,
        ...post_titles.slice(index + 1)
      ];
    }
  }

  return await titlesDoc.ref.update({
    post_titles,
    post_title_count: post_titles.length,
    updated: Date.now()
  });
}

export async function getPostTitlesDoc(userId: string) {
  const doc_path = `users/${userId}/post_titles/titles`
  const titlesDoc = await admin
    .firestore()
    .doc(doc_path)
		.get();
		
  if (!titlesDoc.exists) {
    await titlesDoc.ref.set({ titles: [], updated: Date.now() });
	}
	
  return admin
    .firestore()
    .doc(doc_path)
    .get();
}
