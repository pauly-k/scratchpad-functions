import * as functions from "firebase-functions";
import { setPostTitles, postUnchanged } from "./post.helpers";


export const postCreateHandler = async (
  snapshot: FirebaseFirestore.DocumentSnapshot,
  context: functions.EventContext
) => {
  await snapshot.ref.update({
    id: snapshot.id,
    updated: Date.now(),
    change_count: 1
  });

  const userId = context.params.userId
  return setPostTitles(userId, snapshot.id);
};

export const postUpdatedHandler = async (
  change: functions.Change<FirebaseFirestore.DocumentSnapshot>,
  context: functions.EventContext
) => {
  const previousValue = change.before.data() || {};
  const newValue = change.after.data() || {};

  if (postUnchanged(previousValue, newValue)) {
    return null;
  }

  await change.after.ref.update({
    id: change.after.id,
    change_count: (previousValue.change_count || 1) + 1,
    updated: Date.now()
  });

  const userId = context.params.userId
  return setPostTitles(userId, change.after.id);
};

export const postDeletedHandler = async (
  snapshot: FirebaseFirestore.DocumentSnapshot,
  context: functions.EventContext
) => {
  const userId = context.params.userId
  return setPostTitles(userId, snapshot.id, true);
};

